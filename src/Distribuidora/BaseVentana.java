
package Distribuidora;

import java.awt.BorderLayout;
import java.awt.FlowLayout;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextField;

public class BaseVentana extends JFrame{
    
    public void marco(String a,String b,String c,String d,String e,String f)
    {
        this.setSize(400, 400);
        this.setTitle(a);
        this.setLocation(200, 200);
        
        JPanel PMayor = new JPanel(new BorderLayout());
        JPanel PNorte= new JPanel();
        JPanel PCentro= new JPanel();
        JPanel PSur= new JPanel();
        
        FlowLayout fl1=new FlowLayout(FlowLayout.CENTER);
        PNorte.setLayout(fl1);
        
        GridBagLayout gbl1 = new GridBagLayout();
        GridBagConstraints gbc1 = new GridBagConstraints();
        PCentro.setLayout(gbl1);
        
        FlowLayout fl2 = new FlowLayout();
        PSur.setLayout(fl2);
        
        //panel norte
        JLabel ltitulo= new JLabel(b);
        //panel centro
        JLabel lb2= new JLabel(c);
        JLabel lb3= new JLabel(d);
        JLabel lb4= new JLabel(e);
        JLabel lb5= new JLabel(f);
        String[] lista={"Accion","Terror","Animacion","Drama","Comedia","Romance","Suspenso"};
        JComboBox cb= new JComboBox(lista);
        JTextField tx1= new JTextField(25);
        JTextField tx2= new JTextField(25);
        JTextField tx3= new JTextField(25);
        
        //panel sur
        JButton bt1= new JButton ("Aceptar");
        JButton bt2 = new JButton("Cancelar");
        
        //añadir norte
        PNorte.add(ltitulo);
        //
        gbc1.gridx=0;
        gbc1.gridy=0;
        
        gbc1.anchor = GridBagConstraints.WEST;
        PCentro.add(lb2,gbc1);
        
        gbc1.gridwidth=1;
        gbc1.gridy=1;
        PCentro.add(cb,gbc1);
        gbc1.gridy=2;
        PCentro.add(lb3,gbc1);
        gbc1.gridy=3;
        gbc1.gridwidth=5;
        PCentro.add(tx1,gbc1);
        gbc1.gridy=4;
        gbc1.gridwidth=1;
        PCentro.add(lb4,gbc1);
        gbc1.gridy=5;
        gbc1.gridwidth=5;
        PCentro.add(tx2,gbc1);
        gbc1.gridy=6;
        gbc1.gridwidth=1;
        PCentro.add(lb5,gbc1);
        gbc1.gridy=7;
        gbc1.gridwidth=5;
        PCentro.add(tx3,gbc1);
        
        //panel sur
        PSur.add(bt1);
        PSur.add(bt2);
        
        PMayor.add(PNorte, BorderLayout.NORTH);
        PMayor.add(PCentro, BorderLayout.CENTER);
        PMayor.add(PSur, BorderLayout.SOUTH);
        
        this.add(PMayor);
        this.setVisible(true);
        
        
    }
        
              
    
    
}
